from pprint import pprint
def is_in_danger(board, row_index, column_index):
    for index, value in enumerate(board[row_index]):
        if value == 0 or index ==  column_index:
            pass
        else:
            return True
    return False


def place_queen_in_column(board, column_number):
    if column_number >= 8:
        return
    for row_number in range(8):
        board[row_number][column_number] = 1
        if is_in_danger(board,row_number,column_number):
            board[row_number][column_number] = 0
        else: place_queen_in_column(board,column_number+1)
    


board = []
def n_queens(n):
    for i in range(1, int(n+1)):
        row = []
        for j in range(1, int(n+1)):
            row.append(0)
        board.append(row)
    place_queen_in_column(board, 0)
    return board

if __name__ == "__main__":
    board = n_queens(8)
    pprint(board, width=40)